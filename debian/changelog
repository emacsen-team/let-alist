let-alist (1.0.6-4) UNRELEASED; urgency=medium

  [ Nicholas D Steeves ]
  * Drop emacs24 from Enhances (package does not exist in bullseye).

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.6.1, no changes needed.

 -- Nicholas D Steeves <sten@debian.org>  Wed, 28 Jul 2021 18:43:51 -0400

let-alist (1.0.6-3) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 22:27:43 +0900

let-alist (1.0.6-2) unstable; urgency=medium

  * Source only upload
  * Standards-Version: 4.5.0 (no change needed)

 -- Rémi Vanicat <vanicat@debian.org>  Sat, 01 Feb 2020 11:55:12 +0100

let-alist (1.0.6-1) unstable; urgency=medium

  * New upstream version

 -- Rémi Vanicat <vanicat@debian.org>  Wed, 15 Jan 2020 06:28:13 +0100

let-alist (1.0.5-5) unstable; urgency=medium

  * Team upload.
  * Regenerate source package with quilt patches

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 15:47:27 -0300

let-alist (1.0.5-4) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 10:31:48 -0300

let-alist (1.0.5-3) unstable; urgency=medium

  [ Nicholas D Steeves ]
  * Declare compat level 10.
  * Switch to debhelper 10.
  * Change Vcs links to use salsa rather than alioth.
  * Update Maintainer team name and email address.
  * Drop Built-Using because §7.8 of Policy says it should be used
    "exactly when there are license or DFSG requirements to retain full
    source code", and such requirements are not present in this package.

  [ Rémi Vanicat ]
  * Adopt package (Closes: #904234).
  * Standards-Version: 4.1.5.
  * Remove unnecessary dh argument --parallel

 -- Rémi Vanicat <vanicat@debian.org>  Sun, 29 Jul 2018 12:34:25 +0200

let-alist (1.0.5-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with dh-elpa 1.13 to fix byte-compilation with unversioned
    emacs

 -- David Bremner <bremner@debian.org>  Sat, 02 Jun 2018 21:11:41 -0300

let-alist (1.0.5-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright years.
  * Update CHANGELOG patch.
  * Drop d/source/local-options for dgit.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 18 Jun 2017 22:24:56 +0100

let-alist (1.0.4-1) unstable; urgency=medium

  * Debian package initial release. (Closes: #810835)

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 06 Mar 2016 09:18:21 -0700
